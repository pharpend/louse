-- |This module just re-exports its submodules.
module Louse
       ( module Louse.Main
       )
       where

-- The @import X as X@ idiom is a hack for re-exporting stuff.
import Louse.Main as Louse.Main
