-- |Main module for Louse, in library form so other programs can use it.
module Louse.Main where

import Control.Applicative.AltExtra (altConcat)
import Data.ByteString (ByteString)
import qualified Data.ByteString as B
import Data.FileEmbed (embedFile)
import qualified Data.Text.Encoding as T
import Options.Applicative
import System.IO (stdout)
import System.Pager (printOrPage)

-- |The type to which the command line arguments are marshaled.
data Args = License PagerPref
  deriving Show

data PagerPref = PagerOk
               | NoPager
  deriving Show

louseMain :: IO ()
louseMain = customExecParser lousePrefs louseParserInfo
            >>= louseRunArgs

louseRunArgs :: Args -> IO ()
louseRunArgs = \case
  License NoPager -> B.hPut stdout licenseText
  License PagerOk -> printOrPage $ T.decodeUtf8 licenseText

licenseText :: ByteString
licenseText = $(embedFile "LICENSE")

-- |The preferences for our command line argument parser.
lousePrefs :: ParserPrefs
lousePrefs = defaultPrefs { prefDisambiguate = True
                          , prefShowHelpOnError = True
                          }

louseParserInfo :: ParserInfo Args
louseParserInfo =
  infoHelper louseParser
             (mconcat [ fullDesc
                      , progDesc "Command-line bug tracker client"
                      ])

louseParser :: Parser Args
louseParser =
  altConcat $ fmap subparser [ licenseParser
                             ]

-- |Print the license
licenseParser :: Mod CommandFields Args
licenseParser =
    command "license" $
      infoHelper (fmap License pagerFlag)
                 licenseDesc
  where
    pagerFlag =
      flag PagerOk
           NoPager
           (mappend (help "Disable piping to the system pager.")
                    (long "no-pager"))
    licenseDesc =
      mappend fullDesc
              (progDesc "Print out the license (GPL-3).")

-- * Helper combinators

infoHelper :: Parser a -> InfoMod a -> ParserInfo a
infoHelper p i = info (helper <*> p) i
