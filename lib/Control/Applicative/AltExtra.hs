-- |Missing stuff from 'Control.Applicative'
module Control.Applicative.AltExtra where

import Control.Applicative (Alternative(..))

-- |Concatenate a bunch of 'Alternative' values
altConcat :: Alternative f => [f a] -> f a
altConcat = \case
  [] -> empty
  x : xs -> x <|> altConcat xs
